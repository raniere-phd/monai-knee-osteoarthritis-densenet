# MONAI Knee Osteoarthritis DenseNet

This repository is a Machine Learning model using MONAI's DenseNet
to classify osteoarthritis
and uses data derivates from the [Osteoarthritis Initiative](https://nda.nih.gov/oai) [[1](#kevin2020)].
Existing best-published work [[2](#jain2021)] reports 71.74% accuracy with OsteoHRNet.
and
70.23% accuracy with DenseNet-161 [[3](#yong2021)].

Experiments ara available at [Comet](https://www.comet.ml/raniere-silva/monai-knee-osteoarthritis-densenet/).

## Setting Environment with Conda

Run

```
$ conda env create --file environment.yml
```

## Data

The raw data is the screening radiographs from the [Osteoarthritis Initiative](https://nda.nih.gov/oai/) (OAI). To access the raw data,

1. Create an NIMH Data Archive (NDA) account and accept the [NDA Data Use Certification Terms](https://nda.nih.gov/ndapublicweb/Documents/NDA+Data+Access+Request+DUC+FINAL.pdf).
2. Visit OAI Image Dashboard.
3. Download and "install" Download Manager (Beta).
4. Use Download Manager (Beta) to download `OAIScreeningImages`.

After download `OAIScreeningImages`,
you should have

```
$ ls Package_1192527
enrollee01.txt  NDAR_README.pdf  package_info.txt  results
$ $ unzip -l Package_1192527/results/P001.zip | head -n 5
Archive:  Package_1192527/results/P001.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
    27911  2017-07-10 19:23   0.E.1/9568974/20040614/00138804_1x1.jpg
 13124036  2017-04-11 03:27   0.E.1/9568974/20040614/00138804/001
```

The knee joint location
and
Python script to pre-process the raw data
is available at https://github.com/denizlab/oai-xray-tkr-klg/.

```
$ git clone https://github.com/denizlab/oai-xray-tkr-klg/
```

After Git clone the repository,
follow the instructions in the repository's `README.md`
to extract the knee joints.

The annotation about each knee joint is available in `TestSets`
from https://github.com/denizlab/oai-xray-tkr-klg/.

## Machine Learning Experiment

Run

```
$ python main.py
```

Metrics will be save in [Comet](https://www.comet.ml/).

Run

```
$ python main.py --help
```

to check available arguments.

## Files and Folders

- `agent.py`

  Has the knowledge to handle the data and model for train, validation and test.
- `data`

  Store the data. It should be replace with a symbolic link to the data.
- `data.ipynb`

  Describe the data.
- `dataset.py`

  Has the knowledge to load the data and create batches.
- `environment.yml`

  Conda environment.
- `main.py`

  Runnable script. It stores some hyperparameters.
- `model.py`

  Has the implementation of the model using PyTorch.
- `weights`

  Store the weights of trained model.

## References

<a name='kevin2020'></a>
[1] Leung, Kevin, et al. ‘Prediction of Total Knee Replacement and Diagnosis of Osteoarthritis by Using Deep Learning on Knee Radiographs: Data from the Osteoarthritis Initiative’. Radiology, vol. 296, no. 3, Radiological Society of North America, Sept. 2020, pp. 584–93. pubs.rsna.org (Atypon), https://doi.org/10.1148/radiol.2020192091.

<a name="yong2021"></a>
[3] Yong, Ching Wai, et al. ‘Knee Osteoarthritis Severity Classification with Ordinal Regression Module’. Multimedia Tools and Applications, Jan. 2021. Springer Link, https://doi.org/10.1007/s11042-021-10557-0.

<a name="jain2021"></a>
[2] Jain, Rohit Kumar, et al. Knee Osteoarthritis Severity Prediction Using an Attentive Multi-Scale Deep Convolutional Neural Network. 27 June 2021. http://arxiv.org/abs/2106.14292.