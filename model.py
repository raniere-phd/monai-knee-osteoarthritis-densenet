"""
Model

Include the definition of the model.
"""
import torch
import torch.nn as nn

from monai.networks.nets import DenseNet121

class Model(DenseNet121):
    """Model"""

    def __init__(self):
        """Create instance of model."""
        super().__init__(
            spatial_dims=2,
            in_channels=1,
            out_channels=2
        )
