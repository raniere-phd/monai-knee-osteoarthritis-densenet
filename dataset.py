"""
Dataset

Includes all the functions that either preprocess or postprocess data.
"""
import logging

import os
import os.path

import h5py

import numpy as np

import pandas as pd

import torch.utils.data
import torchvision.io
import torchvision.transforms
import torch.nn

import monai.transforms

logger = logging.getLogger("pytorch.dataset")

TRANSFORMS = {
    'minimal': monai.transforms.Compose([
        monai.transforms.AddChannel(),  # convert into the required channel-first format
        monai.transforms.ScaleIntensity(),  # Scale the intensity between 0 and 1.
        monai.transforms.Resize([224, 224]),
    ]),
    'minimal-448': monai.transforms.Compose([
        monai.transforms.AddChannel(),
        monai.transforms.ScaleIntensity(),
        monai.transforms.Resize([448, 448]),
    ]),
    'minimal-896': monai.transforms.Compose([
        monai.transforms.AddChannel(),
        monai.transforms.ScaleIntensity(),
        monai.transforms.Resize([896, 896]),
    ]),
    'normalization': monai.transforms.Compose([
        monai.transforms.AddChannel(),
        monai.transforms.ScaleIntensity(),
        monai.transforms.Resize([224,224]),
        monai.transforms.NormalizeIntensity(),
    ]),
    'augmentation-train': monai.transforms.Compose([
        monai.transforms.AddChannel(),
        monai.transforms.ScaleIntensity(),
        monai.transforms.Resize([224,224]),
        monai.transforms.RandRotate(range_x=np.pi / 12, prob=0.5, keep_size=True),
        monai.transforms.RandFlip(spatial_axis=0, prob=0.5),
        monai.transforms.RandZoom(min_zoom=0.9, max_zoom=1.1, prob=0.5),
        monai.transforms.EnsureType(),
    ]),
}


class Dataset(torch.utils.data.Dataset):
    """Dataset"""

    def __init__(self, set_ids, transforms):
        """
        :param config:
        """
        self.transforms = transforms

        self.df = pd.concat(
            map(
                pd.read_csv,
                [
                    os.path.join(
                        'sets',
                        f'{i}.csv'
                    ) for i in set_ids
                ]
            ),
            ignore_index=True
        )

    def __len__(self):
        return self.df.shape[0]

    def __getitem__(self, idx):
        image_filename = os.path.join(
            'data',
            '00m',
            self.df.at[idx, 'h5Name']
        )

        with h5py.File(image_filename) as hdf:
            image = hdf['data'][:]
        
        return {
            "image": self.transforms(image),
            "label": self.df.at[idx, 'Label']
        }

    def plot_samples(self):
        """
        Plotting sample images
        """
        raise NotImplementedError

    def make_gif(self):
        """
        Make a gif from a multiple images
        """
        raise NotImplementedError

    def finalize(self):
        raise NotImplementedError


class TrainDataLoader(torch.utils.data.DataLoader):
    """Train data loader"""

    def __init__(self, batch_size, transforms_name, shuffle=True):
        """Create train data loader instance."""
        if transforms_name + "-train" in TRANSFORMS:
            transforms = TRANSFORMS[transforms_name + "-train"]
        else:
            logging.warning(f"Image transformation named '{transforms_name}-train' does NOT exists. Using '{transforms_name}'.")
            if transforms_name in TRANSFORMS:
                transforms = TRANSFORMS[transforms_name]
            else:
                logging.warning(f"Image transformation named '{transforms_name}' does NOT exists. Using 'minimal'.")
                transforms = TRANSFORMS["minimal"]
        
        super().__init__(
            Dataset(
                [1, 2, 3, 4, 5],
                transforms
            ),
            batch_size,
            shuffle=shuffle
        )

class ValDataLoader(torch.utils.data.DataLoader):
    """Validation data loader"""

    def __init__(self, batch_size, transforms_name, shuffle=True):
        """Create validation data loader instance."""
        if transforms_name in TRANSFORMS:
            transforms = TRANSFORMS[transforms_name]
        else:
            logging.warning(f"Image transformation named '{transforms_name}' does NOT exists. Using 'minimal'.")
            transforms = TRANSFORMS["minimal"]
        
        super().__init__(
            Dataset(
                [6],
                transforms
            ),
            batch_size,
            shuffle=shuffle
        )

class TestDataLoader(torch.utils.data.DataLoader):
    """Test data loader"""

    def __init__(self, batch_size, transforms_name, shuffle=True):
        """Create test data loader instance."""
        if transforms_name in TRANSFORMS:
            transforms = TRANSFORMS[transforms_name]
        else:
            logging.warning(f"Image transformation named '{transforms_name}' does NOT exists. Using 'minimal'.")
            transforms = TRANSFORMS["minimal"]
        
        super().__init__(
            Dataset(
                [7],
                transforms
            ),
            batch_size,
            shuffle=shuffle
        )
