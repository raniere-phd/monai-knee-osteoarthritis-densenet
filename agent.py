"""
Agent

Includes all the functions that train, validate, and test the model.
"""
import logging

import datetime

import numpy as np

from sklearn import metrics

import torch
from torch import nn
import torch.optim as optim

import dataset
import model


logger = logging.getLogger("pytorch.agent")


class Agent:
    """
    This base class will contain the base functions to be overloaded by any agent you will implement.
    """

    def __init__(self, hyper_params, no_training=False, comet_logger=None):
        self.epoch = 0
        self.hyper_params = hyper_params
        self.comet = comet_logger
        logger.debug("Creating model ...")
        self.model = model.Model()
        logger.debug("Creating model completed.")
        
        if hyper_params['state-dict']:
            logger.debug(f"Loading weights and biases from {hyper_params['state-dict']} ...")
            self.model.load_state_dict(torch.load(hyper_params['state-dict']))
            logger.debug(f"Loading weights and biases from {hyper_params['state-dict']} completed.")

        logger.debug("Sending model to device ...")
        self.model.to(self.hyper_params["device"])
        logger.debug("Sending model to device completed.")
        
        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.SGD(
            self.model.parameters(),
            lr=self.hyper_params["learning_rate"],
            momentum=0.9
        )
        
        if not no_training:
            logger.debug("Creating data train loader ...")
            self.data_train_loader = dataset.TrainDataLoader(
                self.hyper_params['batch_size'],
                self.hyper_params["transforms_name"]
            )
            logger.debug("Creating data train loader completed.")
            logger.debug("Creating data validation loader ...")
            self.data_validation_loader = dataset.ValDataLoader(
                self.hyper_params['batch_size'],
                self.hyper_params["transforms_name"]
            )
            logger.debug("Creating data train validation completed.")

    def train(self):
        """
        Main training loop
        :return:
        """
        last_epoch_loss = float("inf")
        patience_counter = 0
        
        for epoch in range(self.hyper_params['num_epochs']):
            self.epoch = epoch
            #self.comet.set_epoch(self.epoch)
            
            with self.comet.train():
                logger.info(f"Training (epoch #{self.epoch}) ...")
                
                self.model.train()

                epoch_loss = self.train_one_epoch() / len(self.data_train_loader)

                self.comet.log_metric(
                    'loss',
                    epoch_loss,
                    epoch=self.epoch
                )
                
                logger.info(
                    f"Training (epoch #{self.epoch}) completed with {epoch_loss} running loss."
                )

            with self.comet.validate():
                logger.info(f"Validation after epoch #{self.epoch} ...")
                
                self.validate()
                
                logger.info(f"Validation after epoch #{self.epoch} completed.")
                
            #self.comet.log_epoch_end(epoch)
            
            # Test for earlier stop
            loss_improvement = epoch_loss - last_epoch_loss
            last_epoch_loss = epoch_loss
            
            if loss_improvement > self.hyper_params['min_delta']:
                patience_counter += 1
            else:
                patience_counter = 0
            
            if patience_counter >= self.hyper_params['patience']:
                logger.info("Stopping training earlier based on loss function improvement.")
                break
    
    def train_one_epoch(self):
        """
        One epoch of training
        :return:
        """
        running_loss = 0
        
        for batch_ndx, sample in enumerate(self.data_train_loader):
            logger.debug(f"Training batch #{batch_ndx} ...")
            #self.comet.set_step(batch_ndx)

            inputs, labels = sample.values()
            inputs = inputs.to(self.hyper_params["device"], dtype=torch.float)
            labels = np.asarray(labels)
            labels = torch.from_numpy(labels.astype('long'))
            labels = labels.to(self.hyper_params["device"], dtype=torch.long)

            # zero the parameter gradients
            self.optimizer.zero_grad()

            # forward + backward + optimize
            outputs = self.model(inputs)
            loss = self.criterion(outputs, labels)
            loss.backward()
            self.optimizer.step()

            running_loss += float(loss.item())

            logger.debug(
                f"Training batch #{batch_ndx} completed with {running_loss} running loss."
            )

        return running_loss

    def validate(self):
        """
        Main validate loop
        :return:
        """
        y_true = []
        y_predicted = []
        y_probability_estimation = []

        for batch_ndx, sample in enumerate(self.data_validation_loader):
            logger.debug(f"Validating batch #{batch_ndx} ...")

            inputs, labels = sample.values()
            inputs = inputs.to(self.hyper_params["device"], dtype=torch.float)

            self.model.eval()
            with torch.no_grad():
                outputs = self.model(inputs)
                probability_estimation = torch.nn.functional.log_softmax(
                    outputs,
                    dim=1
                )
                prediction = torch.argmax(
                    probability_estimation,
                    dim=1
                )

            y_true.extend(labels)
            y_predicted.extend(prediction.cpu())
            y_probability_estimation.extend(probability_estimation.cpu())

            logger.debug(f"Validating batch #{batch_ndx} completed.")

        # Need to convert the list of tensors to tensor.
        y_probability_estimation = torch.vstack(
            y_probability_estimation
        )

        logger.info("Saving metrics ...")
        validation_metrics = {
            'accuracy': metrics.balanced_accuracy_score(
                y_true,
                y_predicted
            ),
            'ROC AUC': metrics.roc_auc_score(
                y_true,
                y_probability_estimation[:, -1],
                average=None,
            ),
        }
        logger.info(f'Metrics {validation_metrics}')
        self.comet.log_metrics(
            validation_metrics,
            epoch=self.epoch
        )
        logger.info("Saving metrics completed.")

    def test(self):
        """
        Main test loop
        :return:
        """
        logger.debug("Creating data test loader ...")
        self.data_test_loader = dataset.TestDataLoader(
            self.hyper_params['batch_size'],
            self.hyper_params["transforms_name"]
        )
        logger.debug("Creating data test loader completed.")

        y_true = []
        y_predicted = []
        y_probability_estimation = []

        for batch_ndx, sample in enumerate(self.data_test_loader):
            logger.debug(f"Testing batch #{batch_ndx} ...")

            inputs, labels = sample.values()
            inputs = inputs.to(self.hyper_params["device"], dtype=torch.float)

            self.model.eval()
            with torch.no_grad():
                outputs = self.model(inputs)
                probability_estimation = torch.nn.functional.softmax(
                    outputs,
                    dim=1
                )
                prediction = torch.argmax(
                    probability_estimation,
                    dim=1
                )

            y_true.extend(labels)
            y_predicted.extend(prediction.cpu())
            y_probability_estimation.extend(probability_estimation.cpu())

            logger.debug(f"Testing batch #{batch_ndx} completed.")

        # Need to convert the list of tensors to tensor.
        y_probability_estimation = torch.vstack(
            y_probability_estimation
        )

        logger.info("Saving confusion matrix ...")
        self.comet.log_confusion_matrix(
            y_true,
            y_predicted
        )
        logger.info("Saving confusion matrix completed.")
        logger.info("Saving metrics ...")
        test_metrics = {
            'accuracy': metrics.balanced_accuracy_score(
                y_true,
                y_predicted
            ),
            'ROC AUC': metrics.roc_auc_score(
                y_true,
                y_probability_estimation[:, -1],
                average=None,
            ),
        }
        self.comet.log_metrics(
            test_metrics,
            epoch=self.epoch
        )
        logger.info(f'Metrics {test_metrics}')
        logger.info("Saving metrics completed.")

    def finalize(self):
        """
        Finalizes all the operations
        :return:
        """
        pass

    def save(self):
        """
        Save weights
        :return:
        """
        now = datetime.datetime.now()
        filename = '{:04d}-{:02d}-{:02d}-{:02d}-{:02d}.pth'.format(
            now.year,
            now.month,
            now.day,
            now.hour,
            now.minute
        )
        logger.info(f"Saving model state to {filename} ...")
        torch.save(
            self.model.state_dict(),
            f'weights/{filename}'
        )
        logger.info("Saving model state to {filename} completed.")
