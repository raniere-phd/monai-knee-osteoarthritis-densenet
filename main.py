"""
Main

Manager the agent to train, validate, and test the model.
At the end of the operation,
a report is submited to Comet.
"""
import argparse
import logging

import comet_ml
import torch

from agent import Agent

hyper_params = {
    'device': 'cuda' if torch.cuda.is_available() else 'cpu',
    'num_epochs': None,
    'batch_size': None,
    'learning_rate': None,
    'min_delta': None,
    'patience': None,
    'state-dict': None,
}


def main(keep_local=False, no_training=False, no_state_dict=False):
    """Main loop"""
    logger.info("Starting experiment ...")
    experiment = comet_ml.Experiment(
        project_name="MONAI-Knee-Osteoarthritis-DenseNet",
        workspace="raniere-silva",
        log_code=True,
        log_git_metadata=True,
        log_git_patch=True,
        log_graph=False,
        auto_param_logging=False,
        auto_metric_logging=False,
        auto_output_logging=False,
        auto_log_co2=False,
        disabled=keep_local,
    )
    experiment.log_parameters(hyper_params)
    
    logger.debug("Creating agent ...")
    agent = Agent(
        hyper_params,
        no_training=no_training,
        comet_logger=experiment
    )
    logger.debug("Creating agent completed.")

    # Optimization Loop
    if not no_training:
        agent.train()

    with experiment.test():
        logger.info("Testing model ...")
        agent.test()
        logger.info("Testing model completed.")

    logger.info("Finishing experiment ...")
    experiment.end()
    logger.info("Finishing experiment completed.")

    if not no_state_dict:
        agent.save()

    agent.finalize()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='MONAI Knee Osteoarthritis DenseNet',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    hyper_params_parser = parser.add_argument_group('hyper params')
    hyper_params_parser.add_argument("--batch", type=int, default=32, help="Batch size")
    hyper_params_parser.add_argument("--epochs", type=int, default=16, help="Number of epochs")
    hyper_params_parser.add_argument("--learning-rate", type=float, default=1e-3, help="Learning Rate")
    hyper_params_parser.add_argument("--min-delta", type=float, default=0.1, help="Minimum increase qualify as an improvement")
    hyper_params_parser.add_argument("--patience", type=int, default=3, help="Number of epochs without improvement before stop the training.")

    image_preprocessing = parser.add_mutually_exclusive_group(required=True)
    image_preprocessing.add_argument(
        "--minimal", action="store_true", help="Minimal preprocessing required by model"
    )
    image_preprocessing.add_argument(
        "--minimal-448", action="store_true", help="Minimal preprocessing required by model and resize to 448x448"
    )
    image_preprocessing.add_argument(
        "--minimal-896", action="store_true", help="Minimal preprocessing required by model and resize to 896x896"
    )
    image_preprocessing.add_argument(
        "--normalization", action="store_true", help="Minimal preprocessing required by model followed with normalization"
    )
    image_preprocessing.add_argument(
        "--augmentation", action="store_true", help="Augment images with random rotation, flip and zoom"
    )

    transfer_learning_parser = parser.add_argument_group('transfer learning')
    transfer_learning_parser.add_argument(
        "--no-training", action="store_true", help="Skip training"
    )
    transfer_learning_parser.add_argument(
        "--state-dict", default=None, help="Filename with weights and biases to load into the model"
    )
    transfer_learning_parser.add_argument(
        "--no-state-dict", action="store_true", help="Skip save model state"
    )

    log_level_parser = parser.add_argument_group('log level')
    log_level_parser.add_argument(
        "--debug", action="store_true", help="Set logging level to DEBUG"
    )
    log_level_parser.add_argument(
        "--verbose", action="store_true", help="Set logging level to INFO"
    )
    log_level_parser.add_argument(
        "--local",
        action="store_true",
        help="Disable all network communication with the Comet.ml backend",
    )

    args = parser.parse_args()

    if args.no_training is True and args.state_dict is None:
        parser.error('--state-dict can NOT be None with --no-training')

    logging.basicConfig()
    logger = logging.getLogger("pytorch")
    if args.verbose:
        logger.setLevel(logging.INFO)
    if args.debug:
        logger.setLevel(logging.DEBUG)

    hyper_params["batch_size"] = args.batch
    hyper_params["num_epochs"] = args.epochs
    hyper_params["learning_rate"] = args.learning_rate
    hyper_params["min_delta"] = args.min_delta
    hyper_params["patience"] = args.patience
    hyper_params["state-dict"] = args.state_dict

    if args.minimal:
        hyper_params["transforms_name"] = "minimal"
    if args.minimal_448:
        hyper_params["transforms_name"] = "minimal-448"
        if hyper_params['batch_size'] > 8:
            logging.warning("To avoid CUDA out of memory, batch size was reduced to 8.")
            hyper_params['batch_size'] = 8
    if args.minimal_896:
        hyper_params["transforms_name"] = "minimal-896"
        if hyper_params['batch_size'] > 2:
            logging.warning("To avoid CUDA out of memory, batch size was reduced to 2.")
            hyper_params['batch_size'] = 2
    if args.normalization:
        hyper_params["transforms_name"] = "normalization"
    elif args.augmentation:
        hyper_params["transforms_name"] = "augmentation"
    
    main(
        args.local,
        args.no_training,
        args.no_state_dict
    )
